<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmissionRequest extends Model
{
    protected $table = "admission_request";

}
