<?php

namespace App\Http\Controllers\trastaven;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Users;


class AccountController extends Controller
{
	public function signupService(Request $req)
	{
		$name = $req->input('name');
		$uname = $req->input('uname');
		$password = $req->input('password');

		$hashpassword = Hash::make($password);

		$timenow = Carbon::now(); 
       
        $users = Users::where('user_name',$uname)->first();
        
		  if(empty($users)) {  
		        $user = new Users();
		        $user->name = $name;
		        $user->user_name = $uname;
		        $user->user_password = $hashpassword;
		        $user->user_email = $uname;
		        $user->user_status = 1;
		        $user->created_at = $timenow;
		        $user->updated_at = $timenow;
		        $user->save();

		        \Session::flash('message', "Account has been created"); 
				\Session::flash('alert-class', 'alert-success'); 

		        return redirect('/login');
		    }else{

		    	\Session::flash('message', "Already You've Account. Try to Login"); 
				\Session::flash('alert-class', 'alert-danger'); 
				return redirect()->back();
		    }

	}
    public function loginService(Request $req)
    {
    	$uname = $req->input('uname');
    	$password = $req->input('password');

    	$users = Users::where('user_name',$uname)->first();

    	if (empty($users)) {
    		    
    		    \Session::flash('message', "You don't have account. Please create first"); 
				\Session::flash('alert-class', 'alert-danger'); 
				return redirect()->back();    		
    	}else{

    		    $user_pass = $users['user_password'];

    		          if(Hash::check($password,$user_pass)){
                          
                          session(['uname' => $uname]);
                          session(['name' =>  $users['name']]);
                          session(['u_id' => $uname]);

    		          	  return redirect('/blog');
    		          }else{

    		          	\Session::flash('message', "Password is incoreect"); 
						\Session::flash('alert-class', 'alert-danger'); 
						return redirect()->back();  
    		          }
    	}

	}
	
	public function logout()
	{
		\Session::flush('u_id');

		return redirect('login');
	}
}
