<?php

namespace App\Http\Controllers\trastaven;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Posts;
use App\Users;

class ContactController extends Controller
{

    public function showContactList()
    {
        return view('trastaven.pages.contactlist');
    }

}