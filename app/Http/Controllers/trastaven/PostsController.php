<?php

namespace App\Http\Controllers\trastaven;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Posts;
use App\Users;

class PostsController extends Controller
{
    public function saveBlog(Request $req)
    {
    	$submit_type = $req->input('submit_type');
        
        $post_title = $req->input('post_title');
        $post_content = $req->input('post_content');
        $post_excerpt = $req->input('post_excerpt');

        $post_categories = $req->input('post_categories');
        $post_tags = $req->input('post_tags');
        $post_slug = str_slug($post_title, '-'); //str_replace(" ","-",$post_title);
            

        $post_author_uname = $req->session()->get('u_id');

       			 $post_author_id = Users::where('user_name',$post_author_uname)->first();

        $post_author = $post_author_id['id'];

         $featured_img = $req->file('featured_img');
         $extension = $featured_img->getClientOriginalExtension();	
         $img_name = $featured_img->getClientOriginalName();
		 \Storage::disk('public')->put($img_name,  \File::get($featured_img));

		// echo $featured_img->getClientMimeType();
		// echo $featured_img->getClientOriginalName(); die;
		// echo $post_title.'.'.$extension;
		
        $post_time = Carbon::now(); 

    	if ($submit_type == "draft") {
    		
             $post_save = new Posts();
             $post_save->post_author  = $post_author;
             $post_save->post_title   = $post_title;
             $post_save->post_featured_img  = $img_name;
             $post_save->post_excerpt   = $post_excerpt;
             $post_save->post_status    = $submit_type;
             $post_save->post_content   = $post_content;
             $post_save->post_date      = $post_time;
             $post_save->comment_status =  0;
             $post_save->comment_count  =  0;
             $post_save->post_slug      =  $post_slug;
             $post_save->categories     =  $post_categories;
             $post_save->tags           =  $post_tags;
             $post_save->created_at     =  $post_time;
             $post_save->updated_at     =  $post_time;
             $post_save->save();
             \Session::flash('message', "Blog has been created"); 
             \Session::flash('alert-class', 'alert-success');
             return redirect()->back();
    	}
    	else{

			 $post_save = new Posts();
             $post_save->post_author  = $post_author;
             $post_save->post_title   = $post_title;
             $post_save->post_featured_img  = $img_name;
             $post_save->post_excerpt   = $post_excerpt;
             $post_save->post_status    = $submit_type;
             $post_save->post_content   = $post_content;
             $post_save->post_date      = $post_time;
             $post_save->comment_status =  0;
             $post_save->comment_count  =  0;
             $post_save->post_slug      =  $post_slug;
             $post_save->categories     =  $post_categories;
             $post_save->tags           =  $post_tags;
             $post_save->created_at     =  $post_time;
             $post_save->updated_at     =  $post_time;
             $post_save->save();
             \Session::flash('message', "Blog has been created"); 
				\Session::flash('alert-class', 'alert-success');
             return redirect()->back();
          
    	}
    }

    public function blogList(){

        $blog_data = Posts::orderBy('created_at', 'desc')->paginate(10);

        return view('trastaven.pages.bloglist',compact('blog_data'));
    }

    public function editBlog($id){
        $blog_data = Posts::where('id',$id)->first();

        return view('trastaven.pages.editblog',compact('blog_data'));
    }

    public function updateBlog(Request $req)
    {
        // cross Check the author-id
        $post_author_uname = $req->session()->get('u_id');

                    $post_author_id = Users::where('user_name',$post_author_uname)->first();

        $id = $req->input('id');
        if(empty($post_author_id)){

            \Session::flash('message', "No author is found"); 
            \Session::flash('alert-class', 'alert-danger');
            return redirect()->back();

        }else{
            // update with respect to the author-id
            $submit_type = $req->input('submit_type');
        
            $post_title = $req->input('post_title');
            $post_content = $req->input('post_content');
            $post_excerpt = $req->input('post_excerpt');
    
            $post_categories = $req->input('post_categories');
            $post_tags = $req->input('post_tags');
            $post_slug = str_slug($post_title, '-'); //str_replace(" ","-",$post_title);
            $featured_img = $req->file('featured_img');
            $post_time = Carbon::now();
            if(!empty($featured_img)){
                $extension = $featured_img->getClientOriginalExtension();	
                $img_name = $featured_img->getClientOriginalName();
                \Storage::disk('public')->put($img_name,  \File::get($featured_img));
            
            
                                    if ($submit_type == "draft") {
                                    
                                        $post_save = Posts::find($id);
                                        
                                        $post_save->post_title   = $post_title;
                                        $post_save->post_featured_img  = $img_name;
                                        $post_save->post_excerpt   = $post_excerpt;
                                        $post_save->post_status    = $submit_type;
                                        $post_save->post_content   = $post_content;
                                        $post_save->comment_status =  0;
                                        $post_save->comment_count  =  0;
                                        $post_save->post_slug      =  $post_slug;
                                        $post_save->categories     =  $post_categories;
                                        $post_save->tags           =  $post_tags;
                                        $post_save->updated_at     =  $post_time;
                                        $post_save->save();
                                        \Session::flash('message', "Blog has been updated"); 
                                        \Session::flash('alert-class', 'alert-success');
                                        return redirect()->back();
                                }
                                else{
                        
                                        $post_save = Posts::find($id);
                                        
                                        $post_save->post_title   = $post_title;
                                        $post_save->post_featured_img  = $img_name;
                                        $post_save->post_excerpt   = $post_excerpt;
                                        $post_save->post_status    = $submit_type;
                                        $post_save->post_content   = $post_content;
                                        $post_save->comment_status =  0;
                                        $post_save->comment_count  =  0;
                                        $post_save->post_slug      =  $post_slug;
                                        $post_save->categories     =  $post_categories;
                                        $post_save->tags           =  $post_tags;
                                        $post_save->updated_at     =  $post_time;
                                        $post_save->save();
                                        \Session::flash('message', "Blog has been updated"); 
                                        \Session::flash('alert-class', 'alert-success');
                                        return redirect()->back();
                                    
                                }

                     }else{

                        if ($submit_type == "draft") {
                                    
                            $post_save = Posts::find($id);

                            
                            $post_save->post_title   = $post_title;
                            $post_save->post_excerpt   = $post_excerpt;
                            $post_save->post_status    = $submit_type;
                            $post_save->post_content   = $post_content;
                            $post_save->comment_status =  0;
                            $post_save->comment_count  =  0;
                            $post_save->post_slug      =  $post_slug;
                            $post_save->categories     =  $post_categories;
                            $post_save->tags           =  $post_tags;
                            $post_save->updated_at     =  $post_time;
                            $post_save->save();
                            \Session::flash('message', "Blog has been updated"); 
                            \Session::flash('alert-class', 'alert-success');
                            return redirect()->back();
                    }
                    else{
            
                            $post_save = Posts::find($id);
                            
                            $post_save->post_title   = $post_title;
                            $post_save->post_excerpt   = $post_excerpt;
                            $post_save->post_status    = $submit_type;
                            $post_save->post_content   = $post_content;
                            $post_save->comment_status =  0;
                            $post_save->comment_count  =  0;
                            $post_save->post_slug      =  $post_slug;
                            $post_save->categories     =  $post_categories;
                            $post_save->tags           =  $post_tags;
                            $post_save->updated_at     =  $post_time;
                            $post_save->save();
                            \Session::flash('message', "Blog has been updated"); 
                            \Session::flash('alert-class', 'alert-success');
                            return redirect()->back();
                        
                    }
                     }
             }
                    
        
        }

        public function ListOfBlogs()
        {
            return view('trastaven.frontend.pages.blogs');
        }

        public function singleBlog()
        {
            return view('trastaven.frontend.pages.singleblog');
        }

        public function deleteBlog($id)
        {
            $post = Posts::find($id);

            $post->delete();
            \Session::flash('message', "Blog has been deleted"); 
             \Session::flash('alert-class', 'alert-warning');
              return redirect()->back();
        }

    
}
