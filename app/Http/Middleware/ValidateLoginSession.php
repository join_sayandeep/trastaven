<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ValidateLoginSession
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
   

    public function handle($request, Closure $next, $guard = null)
    {
       
           
        if ($request->session()->get('u_id') != null) {
            
           return redirect()->back();
        
        }
        return $next($request);

    }
}
