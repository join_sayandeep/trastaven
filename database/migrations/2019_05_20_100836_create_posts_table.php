<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('post_author');

            $table->string('post_title');
            $table->string('post_featured_img');
            $table->string('post_excerpt',155);
            $table->string('post_status');
            $table->string('post_content',999);
            $table->dateTimeTz('post_date');
            $table->integer('comment_status');
            $table->integer('comment_count');
            $table->string('post_slug');

            $table->dateTimeTz('created_at');
            $table->dateTimeTz('updated_at');

            // $table->foreign('post_author')->references('id')->on('users');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
