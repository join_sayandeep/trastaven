<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('comment_post_id');

            $table->string('comment_author');
            $table->string('comment_author_email');
            $table->dateTimeTz('comment_date');
            $table->string('comment_content',999);
           

            $table->dateTimeTz('created_at');
            $table->dateTimeTz('updated_at');

            // $table->foreign('comment_post_id')->references('id')->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
