
      <!-- NAvbar -->
               <nav class="navbar navbar-expand-lg trastaven-navbar ">
                       
                     <a class="navbar-brand " href="#"><img src="{{ asset('trastaven/frontend/images/logo.png') }}" alt="logo"></a>
                     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
                     </button>
                     <div class="collapse navbar-collapse" id="navbarNavDropdown">
                     <div class="navbar-nav ml-auto">
                        <a class="nav-item nav-link {{ Request::is('/') ? 'navactive' : '' }} " href="/">Home </a>
                        <a class="nav-item nav-link {{ Request::is('courses') ? 'navactive' : '' }}" href="/courses">Admission </a>
                        <a class="nav-item nav-link" href="/">Placement</a>
                        <a class="nav-item nav-link" href="/">Training</a>
                        <a class="nav-item nav-link" href="/">About Us</a>
                        <a class="nav-item nav-link" href="/">Contact</a>
                     </div>
                     </div>
                  </nav>