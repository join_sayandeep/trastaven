<!DOCTYPE html>
<html lang="en">
<head>
      @include('trastaven.frontend.include.headerlinks')
      <title>Trastaven</title>
</head>
<body>
      @include('trastaven.frontend.include.prenavbar')
      @include('trastaven.frontend.include.navbar')

      @yield('content')

      @include('trastaven.frontend.include.footer')
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script>
      // left-slide
//   (function($) {
//     "use strict";

//     // manual carousel controls
//     $('.next').click(function(){ $('.carousel').carousel('next');return false; });
//     $('.prev').click(function(){ $('.carousel').carousel('prev');return false; });
    
// })(jQuery);


// side bar
$(document).ready(function() {
      $('.side-toggler-btn').on('click' , function(e){
            e.stopPropagation();
      $('.side-toggle-bar').addClass('open');
      $('.whole-div').addClass('darken-body');

      });
      $('.side-toggle-bar').on('click' , function(e){
      e.stopPropagation();
      });

      $('body,html').on('click' , function(e){
      $('.side-toggle-bar').removeClass('open');
      $('.whole-div').removeClass('darken-body');
      });
      
})

</script>
</html>