<!-- popup modals -->
      <!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
      Launch demo modal
    </button> -->
    
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter{{$id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header trastaven-modal-header">
            <h5 class="modal-title trastaven-modal-title" id="exampleModalLongTitle">Enquery Now</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #0AA2CC;">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="/admission/request" method="post">
                 <input type="hidden" name="coursename" value="{{$coursename}}">
                 <input type="hidden" name="clgname" value="{{$clgname}}">
                 <input type="hidden" name="id" value="{{$id}}">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <!-- Name -->
                    <label for="recipient-name" class="col-form-label">Name:</label>
                    <input type="text" class="form-control" name="name" id="recipient-name">
                    <!-- Email -->
                    <label for="recipient-email" class="col-form-label">Email Id:</label>
                    <input type="text" class="form-control" name="email" id="recipient-email">
                    <!-- phone number -->
                    <label for="recipient-phone-number" class="col-form-label">Phone Number:</label>
                    <input type="text" class="form-control" name="ph" id="recipient-phone-number">
                  <!-- Address -->
                  <label for="recipient-address" class="col-form-label">Address:</label>
                  <input type="text" class="form-control" name="address" id="recipient-address">
                <!-- messege -->
                <label for="recipient-phone-number" class="col-form-label">Messege:</label>
                <input type="text" class="form-control" name="msg" id="recipient-Messege">
              
                  
                  
                  </div>

              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-trastaven">Save changes</button>
            </form>
          </div>
        </div>
      </div>
    </div>