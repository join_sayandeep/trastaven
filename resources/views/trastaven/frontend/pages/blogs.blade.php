@extends('trastaven.frontend.layouts.homepage')

@section('content')

<div class="container">
            <div class="row">
            <div class="col-sm-12 col-lg-4">
                  <div class="card">
                    <a href="#"><img src="https://cdn-images-1.medium.com/fit/c/152/156/1*hVnCdEtqlQvHS1GTXbo5Xw.jpeg" alt=""></a>
                   
                  
                         <h6>A deep dive into this in JavaScript: why it’s critical to writing good code.                             
                         </h6>
                         <p>Using simple terminology and a real world example, this post explains what this is and why it is useful. <a href="pager.html">Read More</a> </p>
                         
                         <div class="icons">
                               <a href="#"><i class="fas fa-book-reader"></i></a>
                               <a href="#"><i class="far fa-bookmark"></i></a>
                         </div>
                  </div>       
                   
             </div>
            <div class="col-sm-12 col-lg-4">
                        <div class="card">
                              
                                    <a href="#"><img src="https://cdn-images-1.medium.com/fit/c/152/156/1*hVnCdEtqlQvHS1GTXbo5Xw.jpeg" alt=""></a>
                              
                              <div>
                                    <h6>A deep dive into this in JavaScript: why it’s critical to writing good code.                             
                                    </h6>
                                    <p>Using simple terminology and a real world example, this post explains what this is and why it is useful.</p>
                                    <div class="icons">
                                          <a href="#"><i class="fas fa-book-reader"></i></i></a>
                                          <a href="#"><i class="far fa-bookmark"></i></a>
                                    </div>
                              </div>
                        </div>
                       </div>
                       <div class="col-sm-12 col-lg-4">
                              <div class="card">
                                    <div>
                                          <a href="#"><img class="nonhero" src="https://cdn-images-1.medium.com/fit/c/152/156/1*hVnCdEtqlQvHS1GTXbo5Xw.jpeg" alt=""></a>
                                    </div>
                                    <div>
                                          <h6>A deep dive into this in JavaScript: why it’s critical to writing good code.                             
                                          </h6>
                                          <p>Using simple terminology and a real world example, this post explains what this is and why it is useful.</p>
                                          <div class="icons">
                                                <a href="#"><i class="fas fa-book-reader"></i></a>
                                                <a href="#"><i class="far fa-bookmark"></i></a>
                                          </div>
                                    </div>
                              </div>
                             </div>
            </div>
      </div>
      
</body><div class="container">
            <div class="row">
            <div class="col-sm-12 col-lg-4">
             <div class="card">
                   <div>
                         <a href="#"><img src="https://cdn-images-1.medium.com/fit/c/152/156/1*hVnCdEtqlQvHS1GTXbo5Xw.jpeg" alt=""></a>
                   </div>
                   <div>
                         <h6>A deep dive into this in JavaScript: why it’s critical to writing good code.                             
                         </h6>
                         <p>Using simple terminology and a real world example, this post explains what this is and why it is useful.</p>
                         <div class="icons">
                               <a href="#"><i class="fas fa-book-reader"></i></a>
                               <a href="#"><i class="far fa-bookmark"></i></a>
                         </div>
                   </div>
             </div>
            </div>
            <div class="col-sm-12 col-lg-4">
                        <div class="card">
                              <div>
                                    <a href="#"><img src="https://cdn-images-1.medium.com/fit/c/152/156/1*hVnCdEtqlQvHS1GTXbo5Xw.jpeg" alt=""></a>
                              </div>
                              <div>
                                    <h6>A deep dive into this in JavaScript: why it’s critical to writing good code.                             
                                    </h6>
                                    <p>Using simple terminology and a real world example, this post explains what this is and why it is useful.</p>
                                    <div class="icons">
                                          <a href="#"><i class="fas fa-book-reader"></i></a>
                                          <a href="#"><i class="far fa-bookmark"></i></a>
                                    </div>
                              </div>
                        </div>
                       </div>
                       <div class="col-sm-12 col-lg-4">
                              <div class="card">
                                    <div>
                                          <a href="#"><img src="https://cdn-images-1.medium.com/fit/c/152/156/1*hVnCdEtqlQvHS1GTXbo5Xw.jpeg" alt=""></a>
                                    </div>
                                    <div>
                                          <h6>A deep dive into this in JavaScript: why it’s critical to writing good code.                             
                                          </h6>
                                          <p>Using simple terminology and a real world example, this post explains what this is and why it is useful.</p>
                                          <div class="icons">
                                                <a href="#"><i class="fas fa-book-reader"></i></a>
                                                <a href="#"><i class="far fa-bookmark"></i></a>
                                          </div>
                                    </div>
                              </div>
                             </div>
            </div>
      </div>



@endsection