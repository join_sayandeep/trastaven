@extends('trastaven.frontend.layouts.homepage')

@section('content')


<!-- Search Input -->
<div class="container main-search mt-3">
            <div class="input mb-3">
                        <label for="Admissions">Search:</label>
                        <div class="input-group search mb-3">        
                        <input type="text" class="form-control" placeholder="Find your Courses" aria-label="Find your Courses" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                              <span class="input-group-text text-white" id="basic-addon2">Search</span>
                        </div>
                        </div>
                        </div>
                            
      </div>      

<!-- Filter Section -->
<div class="container-fluid">
            <div class="row">
                        <div class="col-sm-12 col-lg-2 side-bar text-left ">
                              <h2>
                                    Filtering
                              </h2>
                              <!-- dropdown -->
                              <div class="btn-group mb-2">
                                    <!-- <button class="btn  btn-secondary dropdown-toggle  dropdown-toggle-split" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Small button
                                    </button>
                                        
                                          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                          </div> -->
                              </div>
                              <!-- select Box -->
                        
                                          <div class="input-group select input-small mb-3">
                                          <div class="input-group-prepend ">
                                          
                                          </div>
                                          <!-- <select class="custom-select" id="inputGroupSelect01">
                                          <option selected>Choose...</option>
                                          <option value="1">One</option>
                                          <option value="2">Two</option>
                                          <option value="3">Three</option>
                                          </select> -->
                                          </div>
                            <!-- 1st Check box -->
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                              <label class="form-check-label" for="defaultCheck1">
                                Price Low to High
                              </label>
                            </div>
                              <!-- 2nd Check box -->
                              <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                    <label class="form-check-label" for="defaultCheck1">
                                      Default checkbox
                                    </label>
                                  </div>

                  
                  
                  
                        </div>
                        <!-- Search Result -->
                        <div class="col-sm-12 col-lg-9 search-reasult ml-md-4 pr-3">
                        @if($course_data != null)

                        @foreach($course_data as $data)                         
                              <div class="result">
                              <h2>{{ $data->college_name }}</h2>
                                    <h3>{{ $data->course_name }}</h3>
                                    <p><i class="fas fa-map-marker-alt"></i>{{ $data->location }} <br> 
                                          <strong>{{ $data->eligibility_criteria }}</strong> <br>
                                         
                                    </p> 
                                        <!-- Button trigger modal -->
                                          <button type="button" class="btn btn-trastaven" data-toggle="modal" data-target="#exampleModalCenter{{$data->id}}">
                                          Enquery Now
                                          </button>
                                 </div>
                                 @include('trastaven.frontend.pages.admissionreqmodal',['clgname'=>$data->college_name,'coursename'=>$data->course_name,'id'=>$data->id])
                              @endforeach 
                                                            

                              @endif
                              
                  
                              </div>
                              
                        </div>
                        <!-- pagination -->
                        <div class="container">
                        <nav aria-label="Page navigation example">
                        {{ $course_data->render("pagination::bootstrap-4") }}
                                    </nav>

                        </div>
    
                        
                      

</div>



@endsection