@extends('trastaven.frontend.layouts.homepage')

@section('content')

                <!--***** MAIN POSTS *******  -->
                <div class="container-fluid main-section mt-4 ">
                  <div class="row psts">
                        <div class="col-sm-12 col-lg-1 d-none d-lg-block sticky-top">

                              <!-- Fixed Buttons -->
                             
                              <a href="#"><i class="fas fa-thumbs-up mr-1"></i></a>
                              <a href="#"><i class="far fa-comment"></i></a>
                              <a href="#"><i class="far fa-bookmark"></i></a>
                              <a href="#"><i class="fab fa-twitter"></i></a>
                               
                        </div>
                        <div class="col-sm-12 col-lg-8  main-bar scrollit">
                                    <h2 class="ml-md-4 ml-1 mb-2 mr-md-4">
                                    The most difficult things about code and how to tackle them
                                    </h2>
                                    <img class="hero" src="https://cdn-images-1.medium.com/max/1200/0*yLaEVLIg1dFOUE5o">
                                    
                                    <!-- Author -->
                                    <div class="d-flex flex-row m-md-4 mb-1  pl-1">
                                          <div class="author-img mr-2">
                                                 <img src="images/user2.png" alt="">
                                          </div>
                                          <div class="author-des mt-1">
                                                   <div class="author d-flex flex-row">
                                                      <p>Matt Zhou</p>       
                                                      <a href="#" class="btn follow ml-3 mb-1">follow</a>
                                                      </div>
                                                      <p> Sep 11, 2018. 7 min read</p>
                                          </div>

                                    </div>
                                    
                                    <div class="post">
                                                <p>
                                                            Many people have become interested in learning to code in recent years.
                        
                                                            They either find their way into programming through online courses, or through offline meet ups, or are just simply trying to give it a shot.
                                                            
                                                            Websites like code.org, codecademy and freeCodeCamp are becoming more and more popular. There are a huge number of coding courses out on the web, and also available on YouTube.
                                                            
                                                            But coding isn’t easy. Here a some of the challenges we all face when learning to code
                                                     </p>
                                                     <h4>
                                                            1. Finding the “right” amount of time to code everyday.
                        
                                                     </h4>
                                                     <p>
                                                            If you are learning to code by yourself, chances are you have other responsibilities in life.
                        
                                                            You could have a part time job, or a full time job, or you can be a stay at home parent. The point is, everyone is busy in this life. So how do you find the time to code everyday?
                                                            
                                                            Some people may say: “Well, if you are dedicated enough, you can always find time.” True. I agree with that.
                                                            
                                                            So then the question becomes: “How much time should you dedicate everyday to code? If I can only get half an hour per day, does that still count?”
                                                            
                                                            The bottom line is this: only you yourself know how much you can code everyday, and making it a habit of doing it, without getting burnt out. The last part is really important. freeCodeCamp founder Quincy Larson once said on his twitter feed:
                                                     </p>
                                    <h4 class="qoute">
                                                “It is not about your daily progress, it is about progress daily.”
                                    </h4>                 
                                    <p>
                                               
                                                
                                                   
                                    </p> 
                                    <h4>
                                          2. Finding the balance between “not making a good enough progress” and “getting burnt out.”
                                    </h4> 
                                    <p>
                                         
                                   </p>  
                                   <div class="clearfix">
                                         <p class="float-left" >
                                                Here is a video about a senior developer who has been in the tech field for decades talking about how much programming programmers do each day while they are at work.

                                                It is not gonna be the golden standard, but it will give you an idea about how to set yourself a realistic, and most importantly, sustainable plan when it comes to learning to code everyday.
                                                For me personally, I struggled with this a lot.

                                                There are days I just could not understand a single concept/code snippet from the book I was reading.  
                                                that I’d have to calm myself down, go to the balcony, and take a deep breath.

                                                From that point onwards I would keep reminding myself not to overwork it to the point that there was no coming back.
                                                
                                                Programming is not easy. It requires you to concentrate, especially when you are learning new stuff. It is mentally taxing, and there are times that you can’t figure it out — why your code didn’t work, or even why it did.
                                         </p>       
                                                
                                         
                                         <img class="nonhero float-right" src="https://cdn-images-1.medium.com/max/800/1*Qfreh-ivnIQ_Alt7zm9-Aw.jpeg" alt="">

                                    </div>
                                   <p>
                                          I found I was most productive whenever I was really concentrated on the problem I was working on right then, but at the same time I was really relaxed, enjoying the whole process.

                                          This was when I:
                                   </p> 
                               
                                   <ul class="post-list">
                                         <li>Found an issue I needed to solve.</li>
                                         <li>Found the solution through online forums.</li>
                                         <li>Tried a bunch of different ways to solve it just to see which one worked.</li>
                                         <li>I solved the issue altogether.</li>
                                   </ul>
                                   <p>
                                          To cope with the fact that a lot of the stuff we are learning is quite mundane and complex (data structures and algorithms and such), I have developed this 50/50 rule whenever I am learning to code.

                                          <strong>I use 50% of the time to do the difficult tasks, studying the basics</strong> , concepts, algorithms and such. The other 50% of the time I am doing my own projects, projects I am really passionate about. So that there is a balance when it comes to my day-to-day study.
                                   </p>
                                    </div>

<!-- Blog page tags -->
                  <div class="container tag-section ">
                        <ul>
                              <li>
                                 <a href="#">Coding</a>
                              </li>
                              <li>
                              <a href="#">Web development</a>
                              </li>
                              <li>
                              <a href="#">Bootcamp</a>
                              </li>
                              <li>
                              <a href="#">Seo</a>
                              </li>
                              <li>
                              <a href="#">Web development</a>
                              </li>
                              <li>
                              <a href="#">Bootcamp</a>
                              </li>
                        </ul>
                  </div>




                  <!-- Users comments goes here -->

            <!-- end of post column -->
            
                   




                  </div>

            </div>  
            </div>
            




@endsection