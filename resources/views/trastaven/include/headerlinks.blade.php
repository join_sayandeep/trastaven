<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Think Again Lab">
  <title>Trastaven Dashboard</title>
  <!-- Favicon -->
  <link href="{{ asset('trastaven/img/brand/favicon.png') }}" rel="icon" type="image/png">
  <!--  <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=qp8siq6adbd9fafuuylzehh8w773yre90kkfaa27w9xhdl43"></script> -->
  <!-- <script>tinymce.init({selector:'textarea'});</script> -->
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ asset('trastaven/vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
  <link href="{{ asset('trastaven/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('trastaven/css/argon.css?v=1.0.0') }}" rel="stylesheet">
  <link type="text/css" href="{{ asset('trastaven/css/custom.css') }}" rel="stylesheet">

  <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=qp8siq6adbd9fafuuylzehh8w773yre90kkfaa27w9xhdl43"></script>
  <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
  <script>
    tinymce.init({
      selector:'#editor',
      plugins: "image",
      toolbar: "image",
      image_caption: true
      
   });
   </script>
</head>