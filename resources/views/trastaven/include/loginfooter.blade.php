<footer class="py-5">
    <div class="container">
      <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-4">
          
        </div>
        <div class="col-xl-4" style="padding: 0px;">
          <div class="text-white">
            &copy; @php echo date("Y"); @endphp  <a href="#" class="font-weight-bold text-white ml-1" target="_blank">Trastaven</a>. Powered by <a href="#" class="font-weight-bold text-white ml-1" target="_blank">Think Again Lab</a>
          </div>
        </div>
        <div class="col-xl-4">
          <!-- <ul class="nav nav-footer justify-content-center justify-content-xl-end">
            <li class="nav-item">
              <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
            </li>
            <li class="nav-item">
              <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
            </li>
            <li class="nav-item">
              <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
            </li>
            <li class="nav-item">
              <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
            </li>
          </ul> -->
        </div>
      </div>
    </div>
  </footer>