@extends('trastaven.layouts.admissionreqlayout')

@section('content')
<div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="trastavenh3 mb-0">Lists of Admission Requests</h3>
             
            </div>
            
                 
                
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Email-Id</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Address</th>
                    <th scope="col">Message</th>
                    <th scope="col">Institute code</th>
                    <th scope="col">Course Code</th>
                    <th scope="col">Date</th>
                    <!-- <th scope="col"></th> -->
                  </tr>
                </thead>
                <tbody>
                @foreach($admission_req_data as $data)
                  <tr>
                    <th scope="row">
                      <!-- <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="../assets/img/theme/bootstrap.jpg">
                        </a> -->
                        <div class="media-body">
                         <span class="mb-0 text-sm">{{ $data->name }}</span>
                         
                        </div>
                      <!-- </div> -->
                    </th>
                    <td>
                    {{ $data->email }}
                    </td>
                    <td>
                      <span class="badge badge-dot mr-4">
                         {{ $data->phone }} 
                     
                      </span>
                    </td>
                    <td>
                      <div class="avatar-group">
                      {{ $data->address }}
                        
                      </div>
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->msg }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->institute_code }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->course_code }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->date }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>
                    <!-- <td class="text-right">
                      <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <a class="dropdown-item" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Download PDF</a>
                          <a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i>Send in Mail</a>
                          <a class="dropdown-item" href="#"></a>
                        </div>
                      </div>
                    </td> -->
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            {{ $admission_req_data->render("pagination::bootstrap-4") }}
           
          </div>
        </div>
      </div>
     



@endsection