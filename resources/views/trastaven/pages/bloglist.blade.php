@extends('trastaven.layouts.addbloglayout')

@section('content')
<div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
        @if(Session::has('message'))
              <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif
          <div class="card shadow">
            <div class="card-header border-0">
           
              <h3 class="trastavenh3 mb-0">Blogs</h3>
             
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Post Title</th>
                    <th scope="col">Status</th>
                    <th scope="col">Slug</th>
                    <th scope="col">Category</th>
                    <th scope="col">Tag</th>
                    <th scope="col">Date</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                @foreach($blog_data as $data)
                  <tr>
                    <th scope="row">
                      <!-- <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="../assets/img/theme/bootstrap.jpg">
                        </a> -->
                        <div class="media-body">
                         <span class="mb-0 text-sm">{{ $data->post_title }}</span>
                         
                        </div>
                      <!-- </div> -->
                    </th>
                    <td>
                    {{ $data->post_status }}
                    </td>
                    <td>
                      <span class="badge badge-dot mr-4">
                         {{ $data->post_slug }} 
                     
                      </span>
                    </td>
                    <td>
                      <div class="avatar-group">
                      {{ $data->categories }}
                        
                      </div>
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->tags }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->post_date }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>
                    
                    <td class="text-right">
                      <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <a class="dropdown-item" href="/blog/edit/{{ $data->id }}"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Edit</a>
                          <a class="dropdown-item" href="/blog/delete/{{ $data->id }}"><i class="fa fa-" aria-hidden="true"></i>Delete</a>
                        </div>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            {{ $blog_data->render("pagination::bootstrap-4") }}
           
          </div>
        </div>
      </div>
     



@endsection