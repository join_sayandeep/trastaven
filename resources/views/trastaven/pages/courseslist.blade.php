@extends('trastaven.layouts.courselistlayout')

@section('content')
<div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
        @if(Session::has('message'))
              <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif
          <div class="card shadow">
            <div class="card-header border-0">
           
              <h3 class="trastavenh3 mb-0">Courses</h3>
             
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Course Name</th>
                    <th scope="col">Location</th>
                    <th scope="col">College Name</th>
                    <th scope="col">Affiliated</th>
                    <th scope="col">Approved by</th>
                    <th scope="col">Group</th>
                    <th scope="col">Eligibility</th>
                    <th scope="col">Gradetion</th>
                    <th scope="col">First Year &#x20B9;</th>
                    <th scope="col">Second Year &#x20B9;</th>
                    <th scope="col">Third Year &#x20B9;</th>
                    <th scope="col">Fourth Year &#x20B9;</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                @foreach($course_data as $data)
                  <tr>
                    <th scope="row">
                      <!-- <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="../assets/img/theme/bootstrap.jpg">
                        </a> -->
                        <div class="media-body">
                         <span class="mb-0 text-sm">{{ $data->course_name }}</span>
                         
                        </div>
                      <!-- </div> -->
                    </th>
                    <td>
                    {{ $data->location }}
                    </td>
                    <td>
                      <span class="badge badge-dot mr-4">
                         {{ $data->college_name }} 
                     
                      </span>
                    </td>
                    <td>
                      <div class="avatar-group">
                      {{ $data->affiliated_to }}
                        
                      </div>
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->approved_by }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->under_w_group }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>


                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->eligibility_criteria }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>

                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->trastaven_gradetion }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>

                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->first_yr_fees }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>

                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->second_yr_fees }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>

                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->third_yr_fees }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>

                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->fourth_yr_fees }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>

                    
                    <td class="text-right">
                      <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <a class="dropdown-item" href="/course/edit/{{ $data->id }}"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Edit</a>
                          <a class="dropdown-item" href="/course/delete/{{ $data->id }}"><i class="fa fa-" aria-hidden="true"></i>Delete</a>
                        </div>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            {{ $course_data->render("pagination::bootstrap-4") }}
           
          </div>
        </div>
      </div>
     



@endsection