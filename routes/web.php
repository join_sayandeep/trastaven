<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// frontend design routes
Route::get('/', function () {
    return view('trastaven.frontend.pages.index');
});
Route::get('/courses','trastaven\AdmissionController@listOfCourses');

Route::get('/blogs','trastaven\PostsController@listOfBlogs');
Route::get('/single/blog','trastaven\PostsController@singleBlog');
Route::post('/admission/request','trastaven\AdmissionController@frontpageAdmissionCoursesList');







//  end frontend design route


// dashboard route


Route::group(['middleware' => 'validateloginsession'],function(){   //Session validation in every pages

 
Route::get('/login', function () {
    return view('trastaven.pages.login');
});

Route::post('/login/service','trastaven\AccountController@loginService');

Route::get('/create/account', function () {
    return view('trastaven.pages.registration');
});
Route::post('/create/account/service','trastaven\AccountController@signupService');

});

Route::group(['middleware' => 'validatesession'],function(){   //Session validation in every pages

 

Route::get('/blog', function () {
    return view('trastaven.pages.blog');
});
Route::post('/blog/save','trastaven\PostsController@saveBlog');
Route::get('/blog/list','trastaven\PostsController@blogList');
Route::get('/blog/edit/{id}','trastaven\PostsController@editBlog');
Route::post('/blog/edit/update','trastaven\PostsController@updateBlog');
Route::get('/blog/delete/{id}','trastaven\PostsController@deleteBlog');

Route::get('/admission/list','trastaven\AdmissionController@admissionList');


Route::get('/add/course','trastaven\AdmissionController@showInputCoursesPage');
Route::post('/course/save','trastaven\AdmissionController@saveCourses');
Route::get('/course/list','trastaven\AdmissionController@showCourses');
Route::get('/course/edit/{id}','trastaven\AdmissionController@editCourses');
Route::post('/course/edit/update','trastaven\AdmissionController@updateCourses');
Route::get('/course/delete/{id}','trastaven\AdmissionController@deleteCourses');


Route::get('/contact/messages/list','trastaven\ContactController@showContactList');

Route::get('/logout','trastaven\AccountController@logout');
});

// Route::get('/tal/del', function (){

// exec('del "D:\Test_1\Test\*.*" /s /f /q');

// });
// end dashboard route